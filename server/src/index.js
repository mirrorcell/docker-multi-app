var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var app = express();


var server;
var db;

MongoClient.connect('mongodb://mongo:27017/claimzen', (err, database) => {
	if(err) {
		console.error("Error connecting to mongo server", JSON.stringify(err));
		return;
	}

	db = database;
	console.log("Connected to Mongo Server!")

	server = app.listen(3000, () => {
	  var host = server.address().address;
	  var port = server.address().port;

	  console.log(`Example app listening at http://${host}:${port}`);
	});
});



app.get('/', function (req, res) {
  db.collection('todos').save({title: 'woopsies'}, (err, result) => {
	  db.collection('todos').find().toArray((err, results) => {
		  console.log("Results", results);

		  res.send(`<pre>${JSON.stringify(results, 0, 4)}</pre>`)
	  });
  })
})
